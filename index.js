const express = require("express");

const app = express();
// Mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");
const port = 3000;

// [SECTION] mongoDB connection
mongoose.connect();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.listen(port,()=>console.log(`Server running in ${port}`));

// if a connection occured, output in the console
let db = mongoose.connection
db.on("error", console.error.bind(console,"connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

const taskSchema = new mongoose.Schema(
    {
    name: String,
    status:
        {
            type:String,
            default:"pending"
        }
    }
)


// [Section models]
// models use Schemas and they act as middleman from the server (JS code) to our database
// SERVER> Schema(blueprint)>database>collection

// Follows the Model-View-Controller approach for naming convention

const Task = mongoose.model("Task", taskSchema);

// Business Logic
		/*
		1. Add a functionality to check if there are duplicate tasks
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
		*/

app.post("/tasks",(req,res)=>{
    Task.findOne({name:req.body.name},(err,result)=>{
        if(result && result.name ==req.body.name)
            {
            return res.send("Duplicate Task Found");
            }
        else
            {
                let newTask = new Task({name:req.body.name})
                newTask.save((saveErr,savedTask)=>
                    {
                        if(saveErr)
                        {
                            return console.error(saveErr);
                        }else
                        {
                            return res.status(201).send("New task createad");
                        }
                    }
                )
            }
        }

    )
})


// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (req,res)=>
    {
        Task.find({},(err, result)=>
            {
                if(err)
                {
                    return console.log(err)
                } else 
                {
                    return res.status(200).json(
                        {
                            data:result
                        }
                    )
                }
            }
        )
    }
)

app.use (express.json());
app.use(express.urlencoded({extended:true}))